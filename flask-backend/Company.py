import pyrebase
import json
import request
import Item from Item
import FirebaseFunctions as fb

class Company:

   def __init__(self, name, itemList):
      self.name = name
      self.itemList = itemList

   def getCompleteItems(self, name, firebase, user):
      db = firebase.database()
      resp = fb.getSearchItemListByCompany(firebase, user, name)
      dict = {}
      for item in resp.each():
         val = item.val()
	 
